package com.bartek.tickdata;

import com.bartek.common.PricesUtil;
import com.bartek.model.morphia.StockPriceIntraJson;
import com.bartek.mongodb.Mongo;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CreateTickData
{

    public Mongo mongo;

    public CreateTickData()
    {
    }

    // From 1M LocalDate comparison

    public static List<StockPriceIntraJson> create15MTickDataFrom1M(List<StockPriceIntraJson> stockData, LocalDateTime filterToDateTime)
    {

        List<LocalTime> localTimeList = new ArrayList<>();
        for (int i = 9; i <= 16; i++)
        {
            localTimeList.add(LocalTime.of(i, 00, 00));
            localTimeList.add(LocalTime.of(i, 15, 00));
            localTimeList.add(LocalTime.of(i, 30, 00));
            localTimeList.add(LocalTime.of(i, 45, 00));
        }
        localTimeList.add(LocalTime.of(17, 00, 00));
        localTimeList.add(LocalTime.of(17, 15, 00));
        localTimeList.add(LocalTime.of(17, 25, 00));
        // Remove 07:00 (09:00)
        localTimeList.remove(localTimeList.get(0));

        return scaleTickData(localTimeList, stockData, filterToDateTime);

    }

    public static List<StockPriceIntraJson> create1HTickDataFrom1M(List<StockPriceIntraJson> stockData, LocalDateTime filterToDateTime)
    {

        List<LocalTime> localTimeList = new ArrayList<>();
        for (int i = 10; i <= 18; i++)
        {
            localTimeList.add(LocalTime.of(i, 00, 00));
        }

        return scaleTickData(localTimeList, stockData, filterToDateTime);

    }

    public static List<StockPriceIntraJson> create4HTickDataFrom1M(List<StockPriceIntraJson> stockData, LocalDateTime filterToDateTime)
    {
        List<LocalTime> localTimeList = new ArrayList<>();
        localTimeList.add(LocalTime.of(13, 00, 00));
        localTimeList.add(LocalTime.of(17, 00, 00));
        localTimeList.add(LocalTime.of(17, 25, 00));

        return scaleTickData(localTimeList, stockData, filterToDateTime);

    }

    public static List<StockPriceIntraJson> create1DTickDataFrom1D(List<StockPriceIntraJson> stockData, LocalDateTime filterToDateTime)
    {
        List<LocalTime> localTimeList = new ArrayList<>();
        localTimeList.add(LocalTime.of(03, 00, 00));

        return scaleTickData(localTimeList, stockData, filterToDateTime);

    }

    private static BigDecimal findPriceLow(List<StockPriceIntraJson> list)
    {
        return list.stream()
                .map(StockPriceIntraJson::getClose)
                .min(Comparator.naturalOrder())
                .orElse(BigDecimal.ZERO);
    }

    private static StockPriceIntraJson findStockDataIntraJsonLow(List<StockPriceIntraJson> list, BigDecimal priceLow)
    {
        if (!priceLow.equals(0))
        {
            StockPriceIntraJson stockPriceIntraJsonLow = list.stream()
                    .filter(x -> x.getClose().equals(priceLow))
                    .collect(Collectors.toList()).get(0);
            return stockPriceIntraJsonLow;
        }
        else
        {
            return null;
        }
    }

    private static BigDecimal findPriceHigh(List<StockPriceIntraJson> list)
    {
        return list.stream()
                .map(StockPriceIntraJson::getClose)
                .max(Comparator.naturalOrder())
                .orElse(BigDecimal.ZERO);
    }

    private static StockPriceIntraJson findStockDataIntraJsonHigh(List<StockPriceIntraJson> list, BigDecimal priceHigh)
    {
        if (!priceHigh.equals(0))
        {
            StockPriceIntraJson stockPriceIntraJsonHigh = list.stream()
                    .filter(x -> x.getClose().equals(priceHigh))
                    .collect(Collectors.toList()).get(0);
            return stockPriceIntraJsonHigh;
        }
        else
        {
            return null;
        }
    }

    private static StockPriceIntraJson findLastInList(List<StockPriceIntraJson> list)
    {
        return list.stream().reduce((first, second) -> second).orElse(null);
    }

    private static StockPriceIntraJson returnHighLowStockPriceDataIntraJson(List<StockPriceIntraJson> list)
    {
        BigDecimal priceHigh = findPriceHigh(list);
        BigDecimal priceLow = findPriceLow(list);
        StockPriceIntraJson stockDataIntraJsonHigh = findStockDataIntraJsonHigh(list, priceHigh);
        StockPriceIntraJson stockDataIntraJsonLow = findStockDataIntraJsonLow(list, priceLow);
        StockPriceIntraJson lastInList = findLastInList(list);
        lastInList.setHigh(stockDataIntraJsonHigh.getClose());
        lastInList.setLow(stockDataIntraJsonLow.getClose());

        return lastInList;
    }

    private static List<StockPriceIntraJson> scaleTickData(List<LocalTime> localTimeList,
            List<StockPriceIntraJson> priceList, LocalDateTime filterToDateTime)
    {

        // Create result list for all final scaled tick data per day
        List<StockPriceIntraJson> resultList = new ArrayList<>();

        // If filter is not within range return resultList with null value
        if(filterToDateTime != null) {
            System.out.println("entering toDatetime check");
            LocalDateTime minDateTime = priceList.stream().map(u -> u.getDateTime()).min(LocalDateTime::compareTo).get();
            LocalDateTime maxDateTime = priceList.stream().map(u -> u.getDateTime()).max(LocalDateTime::compareTo).get();
            System.out.println("minDateTime: " + minDateTime);
            System.out.println("maxDateTime: " + minDateTime);

            if(!(filterToDateTime.isAfter(minDateTime) && (filterToDateTime.isBefore(maxDateTime) || filterToDateTime.isEqual(maxDateTime)))) {
                System.out.println("toDateTime is not withing range");
                return null;
            }
        }

        // Get min/max LocalDate
        LocalDate minDate = priceList.stream().map(u -> u.getDateTime().toLocalDate()).min(LocalDate::compareTo).get();
        LocalDate maxDate = priceList.stream().map(u -> u.getDateTime().toLocalDate()).max(LocalDate::compareTo).get();
        System.out.println("Min LocalDate:" + minDate);
        System.out.println("Max LocalDate:" + maxDate);

        // Split priceList per LocalDate and create arraylist holding per day parts sublists
        List<List<StockPriceIntraJson>> aggregatedDailyList = new ArrayList<>();
        for (LocalDate date = minDate; date.isBefore(maxDate) || date.isEqual(maxDate); date = date.plusDays(1))
        {
            LocalDate finalDate = date;
            List<StockPriceIntraJson> dailyPriceList = priceList
                    .stream()
                    .filter(c -> c.getDateTime().toLocalDate().isEqual(finalDate))
                    .collect(Collectors.toList());
            aggregatedDailyList.add(dailyPriceList);
        }


        for (List<StockPriceIntraJson> dailyPriceList : aggregatedDailyList)
        {

            List<List<StockPriceIntraJson>> aggregatedTempList = new ArrayList<>();
            List<StockPriceIntraJson> tempList = new ArrayList<>();

            //System.out.println("localTimeList size: " + localTimeList.size());
            //System.out.println("priceList size: " + dailyPriceList.size());

            int j = 0;
            for (int i = 0; i < dailyPriceList.size(); i++)
            {
                LocalTime currentLocalTime = dailyPriceList.get(i).getDateTime().toLocalTime();
                LocalDateTime currentLocalDateTime = dailyPriceList.get(i).getDateTime();
                BigDecimal close = dailyPriceList.get(i).getClose();

                //System.out.println("localTimeList (cut off time): " + localTimeList.get(j));

                if (currentLocalTime.isBefore(localTimeList.get(j)))
                {
                    System.out.println("time: " + dailyPriceList.get(i).getDateTime());
                    System.out.println("price: " + dailyPriceList.get(i).getClose());
                    tempList.add(dailyPriceList.get(i));
                }
                else
                {
                    System.out.println("Templist size: " + tempList.size());
                    if(tempList.size() < 1) {
                        break;
                    }
                    // Check templist (StockPriceIntraJson:s) for open, high, low and add them to last (stream reduce) StockPriceIntraJson in templist
                    StockPriceIntraJson highLowStockPriceDataIntraJson = returnHighLowStockPriceDataIntraJson(tempList);
                    resultList.add(highLowStockPriceDataIntraJson);

                    tempList.forEach(x -> System.out.println("date: " + x.getDateTime() + ", price: " + x.getClose()));
                    //aggregatedTempList.add(tempList);
                    tempList = new ArrayList<>();
                    tempList.add(dailyPriceList.get(i));
                    if (j < localTimeList.size() - 1)
                    {
                        j++;
                        System.out.println("j: " + j);
                        System.out.println("localTimeList: " + localTimeList.get(j));
                    }
                    else
                    {
                        break;
                    }
                }
                if (i == (dailyPriceList.size() - 1))
                {
                    System.out.println("******************");
                    System.out.println("Last data in priceList");
                    tempList.forEach(x -> System.out.println("date: " + x.getDateTime() + ", price: " + x.getClose()));
                    //aggregatedTempList.add(tempList);
                    StockPriceIntraJson highLowStockPriceDataIntraJson = returnHighLowStockPriceDataIntraJson(tempList);
                    resultList.add(highLowStockPriceDataIntraJson);
                }
            }

        }

        if(resultList.size() > 0)
        {
            // Filter off tickddata after filterToDateTime
            resultList = resultList.stream()
                    .filter(x -> x.getDateTime().isBefore(filterToDateTime) || x.getDateTime().isEqual(filterToDateTime))
                    .collect(Collectors.toList());

            System.out.println("///////////////////// RESULTLIST //////////////////////");
            resultList.forEach(x -> System.out.println(
                    "date: " + x.getDateTime() +
                            ", close: " + x.getClose() +
                            ", high: " + x.getHigh() +
                            ", low: " + x.getLow()
            ));

            // Reverse list
            resultList = PricesUtil.reverseList(resultList);
        }

        return resultList;
    }

    public static void main(String[] args) throws IOException
    {
        CreateTickData createTickData = new CreateTickData();
        //createTickData.create15MTickDataFrom1M("PCELL", LocalDateTime.of(2020, 06, 24, 17, 30, 0));
        PricesUtil.createTickData("PCELL", "15M", LocalDateTime.of(2020, 8, 3, 17, 15, 0));
    }
}
