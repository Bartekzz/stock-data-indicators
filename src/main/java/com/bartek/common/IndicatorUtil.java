package com.bartek.common;

import com.bartek.model.morphia.indicators.Cci;
import com.bartek.model.morphia.indicators.WavePm;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class IndicatorUtil
{

    public static WavePm createWavePmPojo(List<BigDecimal> wavePmValue, String period, LocalDateTime toDateTime)
    {

        WavePm wavePm = new WavePm();
        wavePm.setDate(toDateTime);
        wavePm.setPeriod(period);
        wavePm.setWavePm14(wavePmValue.get(0));
        wavePm.setWavePm50(wavePmValue.get(1));
        wavePm.setWavePm100(wavePmValue.get(2));
        wavePm.setWavePm200(wavePmValue.get(3));
        List<WavePm> wavePmList = new ArrayList();
        wavePmList.add(wavePm);

        return wavePm;
    }

    public static Cci createCciPojo(List<BigDecimal> cciValue, String period, LocalDateTime toDateTime)
    {
        Cci cci = new Cci();
        cci.setDate(toDateTime);
        cci.setPeriod(period);
        cci.setCci14(cciValue.get(0));
        cci.setCci50(cciValue.get(1));
        cci.setCci100(cciValue.get(2));
        cci.setCci200(cciValue.get(3));
        return cci;
    }
}
