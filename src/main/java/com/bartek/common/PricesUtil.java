package com.bartek.common;

import com.bartek.helper.ValidateStockPrices;
import com.bartek.model.common.MarketHoliday;
import com.bartek.model.morphia.StockDataIntraJson;
import com.bartek.model.morphia.StockPriceIntraJson;
import com.bartek.mongodb.Morphia;
import com.bartek.tickdata.CreateTickData;
import com.bartek.util.SerializationUtil;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PricesUtil
{

    public static List<StockPriceIntraJson> getStockData(String symbol) throws IOException
    {
        // Validate stock prices = delete invalid prices.
        List<StockPriceIntraJson> invalidPricesList = ValidateStockPrices.getInvalidPrices(symbol);
        ValidateStockPrices.deleteInvalidPrices(invalidPricesList, symbol);

        // Filter price from date after gap if there is one. If missing return null.
        StockDataIntraJson stockDataIntraJson = Morphia.getOneStockDataIntraJson(symbol);
        if(stockDataIntraJson == null) {
            return null;
        }
        List<StockPriceIntraJson> stockPriceIntraJsonList = stockDataIntraJson.getStockPriceIntraJsonList();
        List<LocalDateTime> filterDistinctDatesFromStockPrices = filterDistinctDatesFromStockPrices(stockPriceIntraJsonList);
        List<LocalDate> getTradingDaysWithoutPrice = getTradingDaysWithoutPrice(filterDistinctDatesFromStockPrices);

        if(getTradingDaysWithoutPrice.size() > 0) {
            System.out.println("Missing days!");
            return filterPricesFromDate(stockPriceIntraJsonList, getTradingDaysWithoutPrice.get(getTradingDaysWithoutPrice.size() - 1));
        } else  {
            System.out.println("No missing days.");
            return stockPriceIntraJsonList;
        }
    }

    public static List<StockPriceIntraJson> reverseList(List<StockPriceIntraJson> list) {
        Collections.reverse(list);
        return list;
    }

    public static List<LocalDateTime> filterDistinctDatesFromStockPrices(List<StockPriceIntraJson> stockPriceIntraJsonList) {
        return stockPriceIntraJsonList.stream()
                .map(StockPriceIntraJson::getDateTime)
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<StockPriceIntraJson> filterPricesFromDate(List<StockPriceIntraJson> stockPriceIntraJsonList, LocalDate fromDate) {
        return stockPriceIntraJsonList.stream()
                .filter(x -> x.getDateTime().toLocalDate().isAfter(fromDate))
                .collect(Collectors.toList());
    }

    public static List<StockPriceIntraJson> createTickData(String symbol, String period, LocalDateTime filterToDateTime)
            throws IOException
    {
        List<StockPriceIntraJson> tickData = null;
        List<StockPriceIntraJson> stockData = PricesUtil.getStockData(symbol);

        if(stockData != null && stockData.size() > 0)
        {
            switch (period)
            {
            case "15M":
                tickData = CreateTickData.create15MTickDataFrom1M(stockData, filterToDateTime.plusMinutes(15));
                break;
            case "1H":
                tickData = CreateTickData.create1HTickDataFrom1M(stockData, filterToDateTime.plusHours(1));
                break;
            case "4H":
                tickData = CreateTickData.create4HTickDataFrom1M(stockData, filterToDateTime.plusHours(4));
                break;
            }
        }
        return tickData;
    }

    public static boolean isMarketHoliday(List<MarketHoliday> marketHolidayList, LocalDate date) {
        return marketHolidayList.stream().anyMatch(x -> x.getDate().equals(date));
    }

    public static List<LocalDate> getTradingDaysWithoutPrice(List<LocalDateTime> localDateTimes) throws IOException
    {
        List<LocalDate> missingGapDays = new ArrayList<>();
        List<MarketHoliday> marketHolidays = SerializationUtil.marketHolidaysFromJson("marketholidays.json");
        for(int i = 1; i < localDateTimes.size(); i++)
        {
            LocalDate currentLocalDate = localDateTimes.get(i).toLocalDate();
            LocalDate previousLocalDate = localDateTimes.get(i - 1).toLocalDate();
            long gapDays = previousLocalDate.until(currentLocalDate.minusDays(1), ChronoUnit.DAYS);
            //System.out.println("Gaps days: " + gapDays);
            if (gapDays > 0)
            {
                for (long j = 1; j <= gapDays; j++)
                {
                    System.out.println("Date: " + previousLocalDate.plusDays(j).toString());
                    int dayOfWeekNumber = previousLocalDate.plusDays(j).getDayOfWeek().getValue();
                    System.out.println("Day of week number: " + dayOfWeekNumber);
                    boolean isMarketHoliday = isMarketHoliday(marketHolidays, previousLocalDate.plusDays(j));
                    System.out.println("Is market holiday: " + isMarketHoliday);

                    if (dayOfWeekNumber < 6 && !isMarketHoliday)
                    {
                        missingGapDays.add(previousLocalDate.plusDays(j));
                    }
                }
            }
        }
        missingGapDays.forEach(x -> System.out.println("Day: " + x.toString()));
        return missingGapDays;
    }

    public static void main(String[] args) throws IOException
    {

        LocalDate date = LocalDate.parse("2020-07-13");
        LocalDate date1 = LocalDate.now();
        System.out.println(date.until(date1.minusDays(1), ChronoUnit.DAYS));


        /*
        List<StockPriceIntraJson> stockPriceIntraJsonList = getStockData("DDM");
        stockPriceIntraJsonList.forEach(x -> System.out.println(x.getDateTime()));
        */
    }
}
