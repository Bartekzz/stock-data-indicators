package com.bartek;

import com.bartek.common.IndicatorUtil;
import com.bartek.common.PricesUtil;
import com.bartek.indicators.CreateCci;
import com.bartek.indicators.CreateWavePm;
import com.bartek.model.common.StockCompany;
import com.bartek.model.common.StockList;
import com.bartek.model.morphia.StockPriceIntraJson;
import com.bartek.model.morphia.indicators.Cci;
import com.bartek.model.morphia.indicators.StockIndicator;
import com.bartek.model.morphia.indicators.WavePm;
import com.bartek.mongodb.Mongo;
import com.bartek.mongodb.Morphia;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CreateStockIndicator
{

    public CreateStockIndicator()
    {
    }

    private static List<WavePm> createWavePmList(List<StockPriceIntraJson> stockPriceIntraJsonList, String period, LocalDateTime toDateTime) {

        BigDecimal wavePmValue14 = CreateWavePm.calculateWavePm(stockPriceIntraJsonList, 14);
        BigDecimal wavePmValue50 = CreateWavePm.calculateWavePm(stockPriceIntraJsonList, 50);
        BigDecimal wavePmValue100 = CreateWavePm.calculateWavePm(stockPriceIntraJsonList, 100);
        BigDecimal wavePmValue200 = CreateWavePm.calculateWavePm(stockPriceIntraJsonList, 200);

        List<BigDecimal> wavePmValueList = new ArrayList<>(
                Arrays.asList(wavePmValue14, wavePmValue50, wavePmValue100, wavePmValue200)
        );
        WavePm wavePm = IndicatorUtil.createWavePmPojo(wavePmValueList, period, toDateTime);

        List<WavePm> wavePmList = new ArrayList<>();
        wavePmList.add(wavePm);

        return wavePmList;
    }

    private static List<Cci> createCciList(List<StockPriceIntraJson> stockPriceIntraJsonList, String period, LocalDateTime toDateTime) {

        BigDecimal cciValue14 = CreateCci.calculateCci(stockPriceIntraJsonList, 14);
        BigDecimal cciValue50 = CreateCci.calculateCci(stockPriceIntraJsonList, 50);
        BigDecimal cciValue100 = CreateCci.calculateCci(stockPriceIntraJsonList, 100);
        BigDecimal cciValue200 = CreateCci.calculateCci(stockPriceIntraJsonList, 200);

        List<BigDecimal> cciValueList = new ArrayList<>(
                Arrays.asList(cciValue14, cciValue50, cciValue100, cciValue200)
        );
        Cci cci = IndicatorUtil.createCciPojo(cciValueList, period, toDateTime);

        List<Cci> cciList = new ArrayList<>();
        cciList.add(cci);

        return cciList;
    }

    public static void createStockIndicator(StockCompany stockCompany, String period, LocalDateTime toDateTime)
    {
        System.out.println("Creating stockIndicator for: " + stockCompany.getSymbol());
        System.out.println("Creating stockIndicator for (symbolAlt): " + stockCompany.getSymbol());
        List<StockPriceIntraJson> stockPriceIntraJsonList = null;
        try
        {
            stockPriceIntraJsonList = PricesUtil.createTickData(stockCompany.getSymbol(), period, toDateTime);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        if(stockPriceIntraJsonList != null && stockPriceIntraJsonList.size() > 0)
        {

            System.out.println("stockPriceIntraJsonList size: " + stockPriceIntraJsonList.size());

            List<WavePm> wavePmList = createWavePmList(stockPriceIntraJsonList, period, toDateTime);
            List<Cci> cciList = createCciList(stockPriceIntraJsonList, period, toDateTime);

            StockIndicator stockIndicator = new StockIndicator();
            stockIndicator.setStockCompany(stockCompany);
            stockIndicator.setWavePmList(wavePmList);
            stockIndicator.setCciList(cciList);

            Morphia.updateStockIndicator(stockIndicator);
        }
        else {
            System.out.println("toDateTime is beyond data");
        }
    }

    public static void createStockIndicatorForAllCompanies(String period, LocalDateTime toDateTime) {
        Mongo mongo = Mongo.getMongoClientLocal();
        mongo.getMongoDatabase("stockdata");
        List<StockCompany> stockCompanyList = mongo.getAllCompanies("nasdaqomx");
        Collections.sort(stockCompanyList);

        for (StockCompany stockCompany : stockCompanyList)
        {
            System.out.println("StockCompany symbolAlt: " + stockCompany.getSymbolAlt());
            createStockIndicator(stockCompany, period, toDateTime);
        }
    }

    public static void main(String[] args)
    {
        long startTime = System.nanoTime();


/*        StockCompany stockCompany = new StockCompany();
        stockCompany.setSymbol("ADDT.B");
        stockCompany.setSymbolAlt("ADDT_B");

        createStockIndicator(stockCompany, "15M", LocalDateTime.of(2020, 9, 10, 15, 0, 0));*/


        createStockIndicatorForAllCompanies("15M", LocalDateTime.of(2020, 9, 18, 16, 0, 0));


        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("Execution time in seconds : " +
                timeElapsed / 100000000);
    }
}
