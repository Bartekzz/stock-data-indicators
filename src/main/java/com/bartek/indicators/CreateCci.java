package com.bartek.indicators;

import com.bartek.common.PricesUtil;
import com.bartek.model.morphia.StockPriceIntraJson;
import com.jidesoft.utils.BigDecimalMathUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CreateCci
{
    public CreateCci() {

    }

    public static BigDecimal typicalPrice(BigDecimal close, BigDecimal high, BigDecimal low) {
        return (close.add(high).add(low)).divide(new BigDecimal("3"), MathContext.DECIMAL128)
            .setScale(6, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal typicalPrice(StockPriceIntraJson stockPrice) {
        return (stockPrice.getClose().add(stockPrice.getHigh()).add(stockPrice.getLow()))
                .divide(new BigDecimal("3"), MathContext.DECIMAL128)
                .setScale(6, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal lastTypicalPrice(List<StockPriceIntraJson> list, int limit) {
        BigDecimal b = typicalPrice(Objects.requireNonNull(
                list.stream()
                .limit(limit == 0 ? list.size() : limit)
                .reduce((first, second) -> first)
                .orElse(null))
        );

        System.out.println("lastTypicalPrice: " +  b);

        return b;
    }

    public static BigDecimal meanTypicalPrice(List<StockPriceIntraJson> list, int limit) {
        BigDecimal b = BigDecimalMathUtils.mean(
                list.stream()
                        .limit(limit == 0 ? list.size() : limit)
                        .map(CreateCci::typicalPrice)
                        .collect(Collectors.toList()),
                MathContext.DECIMAL128
        ).setScale(6, BigDecimal.ROUND_HALF_UP);

        System.out.println("meanTypicalPrice: " + b);

        return b;
    }

    public static BigDecimal deviationMeanTypicalPrice(List<StockPriceIntraJson> list, BigDecimal currentMean, int limit) {
        BigDecimal b = list.stream().limit(limit == 0 ? list.size() : limit)
                .map(x -> (typicalPrice(x.getClose(), x.getHigh(), x.getLow()).subtract(currentMean)).abs())
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .abs()
                .divide(BigDecimal.valueOf(BigDecimal.valueOf(limit).intValueExact()), MathContext.DECIMAL128)
                .setScale(6, BigDecimal.ROUND_HALF_UP);

        System.out.println("deviationMeanTypicalPrice: " + b);

        return b;

    }

    public static BigDecimal Cci(BigDecimal lastTypicalPrice, BigDecimal currentMean, BigDecimal deviationMeanTypicalPrice) {
            return (lastTypicalPrice.subtract(currentMean))
                .divide(BigDecimal.valueOf(0.015), MathContext.DECIMAL128)
                .divide(deviationMeanTypicalPrice, MathContext.DECIMAL128)
                .setScale(0, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateCci(List<StockPriceIntraJson> stockPriceIntraJsonList, Integer cciLength)
    {
        // Calculuate Wave-pm only is there is sufficient data (14 records)
        if(stockPriceIntraJsonList.size() >= cciLength)
        {

            BigDecimal meanTypicalPrice = meanTypicalPrice(stockPriceIntraJsonList, cciLength);
            BigDecimal deviationMeanTypicalPrice =
                    deviationMeanTypicalPrice(stockPriceIntraJsonList, meanTypicalPrice, cciLength);
            BigDecimal lastTypicalPrice = lastTypicalPrice(stockPriceIntraJsonList, cciLength);
            BigDecimal cci = Cci(lastTypicalPrice, meanTypicalPrice, deviationMeanTypicalPrice);

            System.out.println("CCI: " + cci);
            return cci;

        }
        else
        {
            return null;
        }
    }

    public static void main(String[] args) throws IOException
    {

    }

}
