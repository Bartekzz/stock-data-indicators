package com.bartek.indicators;

import ch.obermuhlner.math.big.BigDecimalMath;
import com.bartek.model.morphia.StockPriceIntraJson;
import com.jidesoft.utils.BigDecimalMathUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.stream.Collectors;

public class CreateWavePm
{

    public CreateWavePm()
    {
    }

    public static BigDecimal meanClosePrice(List<StockPriceIntraJson> list, int limit)
    {

        //System.out.println("list size: " + list.size());

        List<BigDecimal> l = list.stream()
                .limit(limit == 0 ? list.size() : limit)
                .map(StockPriceIntraJson::getClose)
                .collect(Collectors.toList());

        //System.out.println("last 14 size: " + l.size());
        //l.forEach(x -> System.out.println("last 14 prices: " + x));

        BigDecimal m = BigDecimalMathUtils.mean(l, MathContext.DECIMAL32);
        //System.out.println("m: " + m);
        return m;
    }

    public static BigDecimal sumCurrentMeanDeltaVariance(List<StockPriceIntraJson> list, BigDecimal currentMean,
            int limit)
    {
        return list.stream().limit(limit == 0 ? list.size() : limit)
                .map(x -> x.getClose().subtract(currentMean).pow(2))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal shortDev(BigDecimal bandStd, BigDecimal sumCurrentMeanDeltaVariance, BigDecimal bandLength)
    {
        return bandStd.multiply(BigDecimalMath
                .sqrt(sumCurrentMeanDeltaVariance.divide(bandLength, MathContext.DECIMAL128), MathContext.DECIMAL128));
    }

    public static BigDecimal shortDev1(BigDecimal shortDev, BigDecimal point)
    {
        return shortDev.divide(point).pow(2);
    }

    public static BigDecimal sumShortDev1(List<StockPriceIntraJson> list, int longBandLimit, BigDecimal shortbandLength,
            BigDecimal bandStd, BigDecimal point, int lookback)
    {
        System.out.println("/// sumShortDev1 ///");

        BigDecimal sumShortDev1 = BigDecimal.ZERO;
        //for (int i = 0; i <= longBandLimit - shortbandLength.intValueExact(); i++)
        for (int i = 0; i <= lookback - 1; i++)
        {
            List<StockPriceIntraJson> currentList = list.stream().limit(longBandLimit).skip(i).collect(
                    Collectors.toList());
            BigDecimal currentMean = meanClosePrice(currentList, shortbandLength.intValueExact());
            BigDecimal sumCurrentMeanDeltaVariance =
                    sumCurrentMeanDeltaVariance(currentList, currentMean, shortbandLength.intValueExact());
            BigDecimal shortDev = shortDev(bandStd, sumCurrentMeanDeltaVariance, shortbandLength);
            BigDecimal shortDev1 = shortDev1(shortDev, point);

            if (i < lookback)
            {
                //System.out.println("lookback: " + i);
                sumShortDev1 = sumShortDev1.add(shortDev1);
            }
        }

        return sumShortDev1;
    }

    public static BigDecimal shortDev1SqrtPoint(BigDecimal sumShortDev1, BigDecimal lookback, BigDecimal point)
    {
        System.out.println("/// shortDev1SqrtPoint ///");
        return BigDecimalMath.sqrt(sumShortDev1.divide(lookback), MathContext.DECIMAL128).multiply(point);
    }

    public static BigDecimal devDelta(BigDecimal shortDev, BigDecimal shortDev1SqrtPoint)
    {
        return shortDev.divide(shortDev1SqrtPoint, MathContext.DECIMAL128);
    }

    public static BigDecimal devDeltaTanH(BigDecimal devDelta)
    {
        return BigDecimalMath.tanh(devDelta, MathContext.DECIMAL128);
    }

    public static BigDecimal calculateWavePm(List<StockPriceIntraJson> stockPriceIntraJsonList, Integer waveLength)
    {

        BigDecimal shortBandLength = BigDecimal.valueOf(waveLength);
        int lookback = 100;
        int longBandLimit = lookback + waveLength;
        BigDecimal bandStd = new BigDecimal("1.25");
        BigDecimal point = new BigDecimal("0.2");

        // Calculuate Wave-pm only is there is sufficient data (114 records)
        if (stockPriceIntraJsonList.size() >= longBandLimit)
        {
            // Short band
            System.out.println("/// Calculating short band ///");
            BigDecimal currentMean = meanClosePrice(stockPriceIntraJsonList, waveLength);
            BigDecimal sumCurrentMeanDeltaVariance =
                    sumCurrentMeanDeltaVariance(stockPriceIntraJsonList, currentMean, waveLength);
            BigDecimal shortDev = shortDev(bandStd, sumCurrentMeanDeltaVariance, shortBandLength);

            // Long band
            System.out.println("/// Calculating long band ///");
            BigDecimal sumShortDev1 =
                    sumShortDev1(stockPriceIntraJsonList, longBandLimit, shortBandLength, bandStd, point, lookback);
            System.out.println("sumShortDev1: " + sumShortDev1);
            BigDecimal shortDev1SqrtPoint =
                    shortDev1SqrtPoint(sumShortDev1, new BigDecimal(String.valueOf(lookback)), point);
            BigDecimal devDelta = devDelta(shortDev, shortDev1SqrtPoint);
            BigDecimal devDeltaTanH = devDeltaTanH(devDelta).setScale(6, BigDecimal.ROUND_HALF_UP);

            System.out.println("Wave pm: " + devDeltaTanH);

            return devDeltaTanH;
        }
        else
        {
            //throw new RuntimeException("Not sufficient data points, " + longBandLimit);
            return null;
        }
    }

    public static void main(String[] args) throws Exception
    {

    }
}
